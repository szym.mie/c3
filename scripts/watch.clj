(require '[cljs.build.api :as b])

(b/watch "src"
  {:main 'c3.core
   :output-to "out/c3.js"
   :output-dir "out"})
