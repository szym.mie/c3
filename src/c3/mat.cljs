(ns c3.mat)

(defn normv [v] ; normalize vector
  (let [d (->> (map #(Math/pow % 2) v)
               (vec)
               (reduce +)
               (Math/sqrt))] 
    (vec (map #(/ % d) v))))

(defn mulmv [m v] ; multiply vector by matrix 
  (vec (map #(reduce + (vec (map * % v))) m)))

(defn rotma [a r] ; vector axis rotation to matrix.
  (let [s (Math/sin r)
        c (Math/cos r)
        t (- 1 c)
        [x y z] a 
        txy (* t x y)
        txz (* t x z)
        tyz (* t y z)
        xs (* x s)
        ys (* y s)
        zs (* z s)] 
    [[(+ (* t x x) c) ;; row 0
      (- txy zs)
      (+ txz ys)
      0]
     [(+ txy zs) ;; row 1
      (+ (* t y y) c)
      (- tyz xs)
      0]
     [(- txz ys) ;; row 2
      (+ tyz xs)
      (+ (* t z z) c)
      0]
     [0 ;; row 3
      0
      0
      1]]))

(defn trnma [v] ; vector translation to matrix.
  (let [[x y z _] v] 
    ([[0 ;; row 0
      0
      0
      0]
     [0 ;; row 1
      0
      0
      0]
     [0 ;; row 2
      0
      0
      0]
     [x ;; row 3
      y
      z
      1]])))

(defn cmbma [m0 m1] ; combine matrices
  (let [[r00 r01 r02 _] m0
        [r10 r11 r12 _] m1
        ar (fn [r0 r1] (map + r0 r1))]
    [(ar r00 r10)
     (ar r01 r11)
     (ar r02 r12) 
     [0
      0
      0
      1]]))

(defn mulma [m0 m1] ; multiply matrices
  (let [[r00 r01 r02 _] m0
        [r10 r11 r12 _] m1
        mr (fn [r0 r1] (map * r0 r1))]
    [(mr r00 r10)
     (mr r01 r11)
     (mr r02 r12)
     [0
      0
      0
      1]]))

(defn pertr [o p z c] 
  (-> (+ o p)
      (/ z)
      (* 100)
      (+ c)))
