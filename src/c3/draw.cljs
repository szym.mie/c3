(ns c3.draw 
  (:require [c3.mat :as mat]))

(defn draw3p [c v p b] ;; draw to point with camera transform 
  (let [[x y z _] v
        [xp yp zp _] p
        zc (+ z zp)
        [bw bh] b] (.lineTo c (mat/pertr x xp zc bw) (mat/pertr y yp zc bh))))

(defn draw3m [c v p b] ;; draw to point with camera transform 
  (let [[x y z _] v
        [xp yp zp _] p
        zc (+ z zp)
        [bw bh] b] (.moveTo c (mat/pertr x xp zc bw) (mat/pertr y yp zc bh))))

(defn draw3l [c ip vl p b]
  (let [v (vec (map vl ip))]
    (do (draw3m c (v 0) p b)
        (draw3p c (v 1) p b))))

(defn clear [c b]
  (let [[bw bh] b] (.clearRect c 0 0 (* bw 2) (* bh 2))))

;; draw routine

(defn drawf [c vl wl tm p b]
  (clear c b)
  (.beginPath c)
  (js/console.log "cleared screen")
  (doseq [w wl] (draw3l c w vl p b))
  (js/console.log "frame drawn")
  (.stroke c)
)
