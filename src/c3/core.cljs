(ns c3.core
  (:require [c3.draw :as draw] [c3.mat :as mat]))

(def WID 450)
(def HEI 200)
(def WIDHL (/ WID 2))
(def HEIHL (/ HEI 2))

(def can (.getElementById js/document "main_can"))
(def ctx (.getContext can "2d"))

(def angle (atom (/ Math/PI 60)))
(def angax (atom (mat/normv [1 0.4 0.6])))
(def trmat (atom (mat/rotma @angax @angle)))
(def camof (atom [0 0 5 1]))
(def verts (atom [[1 1 1 1]
                  [-1 1 1 1]
                  [1 -1 1 1]
                  [-1 -1 1 1]
                  [1 1 -1 1]
                  [-1 1 -1 1]
                  [1 -1 -1 1]
                  [-1 -1 -1 1]]))
(def wires (atom [[0 1]
                  [1 3]
                  [0 2]
                  [2 3]
                  [4 5]
                  [5 7]
                  [4 6]
                  [6 7]
                  [0 4]
                  [1 5]
                  [2 6]
                  [3 7]]))

(defn render []
  (draw/drawf ctx (vec (map #(mat/mulmv @trmat %) @verts)) @wires @trmat @camof [WIDHL HEIHL])
  (swap! angle + 0.01)
  (reset! trmat (mat/rotma @angax @angle))
  (js/window.requestAnimationFrame render)
)

(set! (.-fillStyle ctx) "rgb(255,0,0)")
(set! (.-lineWidth ctx) 2)
(render)
